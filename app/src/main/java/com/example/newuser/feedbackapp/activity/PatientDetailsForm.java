package com.example.newuser.feedbackapp.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.newuser.feedbackapp.R;

public class PatientDetailsForm extends AppCompatActivity {

    private EditText name, mobileNo, email;
    private TextView errorName, errorMobileNo, errorEmail;
    private String firstName, mail, mobNo, photo;
    private Button submitButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details_form);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        findViews();

    }


    public void findViews() {
        name = findViewById(R.id.name);
        mobileNo = findViewById(R.id.mobile);
        email = findViewById(R.id.email);

        errorName = findViewById(R.id.error_name);
        errorMobileNo = findViewById(R.id.error_mobile_no);
        errorEmail = findViewById(R.id.error_email);

        submitButton = findViewById(R.id.submit_btn);

    }

//    public void onClickSubmitButton(View view) {
//        if (isValidatedFields()) {
//            Intent intent = new Intent(view.getContext(), RatingScreen.class);
//            intent.putExtra("name", firstName);
//            intent.putExtra("mobNo", mobNo);
//            intent.putExtra("email", mail);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//        }
//    }

    public boolean isValidatedFields() {
        boolean result = true;
        firstName = name.getText().toString().trim();
        if (firstName.length() == 0) {
            errorName.setVisibility(View.VISIBLE);
            result = false;
        } else {
            errorName.setVisibility(View.INVISIBLE);
        }
        mobNo = mobileNo.getText().toString().trim();
        if (mobileNo.length() == 0 || !isValidMobile(mobNo)) {
            errorMobileNo.setVisibility(View.VISIBLE);
            result = false;
        } else {
            errorMobileNo.setVisibility(View.INVISIBLE);
        }

        mail = email.getText().toString().trim();
        boolean isValid = isValidEmail(mail);
        if (mail.length() == 0 || !isValid) {
            errorEmail.setVisibility(View.VISIBLE);
            result = false;
        } else {
            errorEmail.setVisibility(View.INVISIBLE);
        }
        return result;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

