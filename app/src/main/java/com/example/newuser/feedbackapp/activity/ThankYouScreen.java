package com.example.newuser.feedbackapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.newuser.feedbackapp.R;

import java.util.Timer;
import java.util.TimerTask;


public class ThankYouScreen extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        clickImage();
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){
            @Override
            public void run() {
                // TODO Auto-generated method stub
                Intent intent = new Intent(ThankYouScreen.this, SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }}, 800);

    }

    public void clickImage() {
       Intent cameraIntent = new Intent(getApplicationContext(), CameraService.class);
        cameraIntent.putExtra("Front_Request", true);
        cameraIntent.putExtra("Quality_Mode", 100);
        getApplicationContext().startService(cameraIntent);
    }
}
