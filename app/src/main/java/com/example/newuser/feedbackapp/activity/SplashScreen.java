package com.example.newuser.feedbackapp.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.newuser.feedbackapp.R;

public class SplashScreen extends AppCompatActivity implements View.OnClickListener {

    public int count = 0;
    private EditText regID;
    public Button secretAdminButton, submitBtn;
    int id;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        findViews();
    }

    public void findViews() {
        secretAdminButton = (Button) findViewById(R.id.secret_button);
        secretAdminButton.setOnClickListener(this);
        regID = (EditText) findViewById(R.id.ed_id);
        submitBtn = (Button) findViewById(R.id.submit_btn);
    }

    public void enterRegId() {
        id = Integer.valueOf(regID.getText().toString());


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.secret_button:
                if (count == 4) {
                    Intent intent = new Intent(getApplicationContext(), AdminReportActivity.class);
                    startActivity(intent);
                    count = 0;
                }
                count++;
                break;


            case R.id.submit_btn:
                enterRegId();
                if(id != 0){
                   Intent openRatingScreen = new Intent(this, RatingScreen.class);
                   openRatingScreen.putExtra("id", id);
                   startActivity(openRatingScreen);

                }else{
                    regID.setError("Please enter ID first");
                }

                break;

        }
    }
}

