package com.example.newuser.feedbackapp.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.newuser.feedbackapp.R;
import com.example.newuser.feedbackapp.model.PatientDetailsModel;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RatingScreen extends AppCompatActivity {
    DatabaseReference databaseReference;
    DatabaseReference goodReference, avgReference, poorReference;
    public PatientDetailsModel model;
    private String childID;
    private String rating;
    private String timeStamp;
    private int regID;
    Query query;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_screen);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);

        databaseReference = FirebaseDatabase.getInstance().getReference("patientDetails");
        goodReference = databaseReference.child("Good Ratings");
        avgReference = databaseReference.child("Average Ratings");
        poorReference = databaseReference.child("Poor Ratings");
        regID = getIntent().getIntExtra("id", 0);
    }

    public void writeToDB() {
        if (rating.equalsIgnoreCase("Good")) {
            childID = databaseReference.push().getKey();
            goodReference.orderByChild("timeStamp").startAt(timeStamp);
            model = new PatientDetailsModel(regID, rating, timeStamp);
            goodReference.child(childID).setValue(model, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    System.out.println("Value was set. Error = " + databaseError);

                }
            });


        } else if (rating.equalsIgnoreCase("Average")) {
            childID = databaseReference.push().getKey();
            model = new PatientDetailsModel(regID, rating, timeStamp);
            avgReference.child(childID).setValue(model, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    System.out.println("Value was set. Error = " + databaseError);

                }
            });

        } else if (rating.equalsIgnoreCase("Poor")) {
            childID = databaseReference.push().getKey();
            model = new PatientDetailsModel(regID,rating, timeStamp);
            poorReference.child(childID).setValue(model, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    System.out.println("Value was set. Error = " + databaseError);

                }
            });

        }
    }

    public void onClickGoodRatingButton(View view) {
        rating = "Good";
        getTimeStamp();
        writeToDB();
        Intent openThankYouIntent = new Intent(view.getContext(), ThankYouScreen.class);
        startActivity(openThankYouIntent);

    }

    public void onClickAvgRatingButton(View view) {
        rating = "Average";
        getTimeStamp();
        writeToDB();
        Intent openFeedbackIntent = new Intent(view.getContext(), FeedbackFormScreen.class);
        openFeedbackIntent.putExtra("firebaseID", childID);
        openFeedbackIntent.putExtra("fromAvgFeedback", true);
        startActivity(openFeedbackIntent);

    }

    public void onClickPoorRatingButton(View view) {
        rating = "Poor";
        getTimeStamp();
        writeToDB();
        Intent openFeedbcakIntent = new Intent(view.getContext(), FeedbackFormScreen.class);
        openFeedbcakIntent.putExtra("id", regID);
        openFeedbcakIntent.putExtra("fromPoorFeedback", true);
        openFeedbcakIntent.putExtra("firebaseID",childID);
        startActivity(openFeedbcakIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getTimeStamp() {
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm a");
        String date = df.format(new Date());
        timeStamp = date;
    }
}
