package com.example.newuser.feedbackapp.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.example.newuser.feedbackapp.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FeedbackFormScreen extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private EditText comments;
    private String commentsText, childID;
    DatabaseReference databaseReference;
    int id;
    private RadioGroup radioGroup1, radioGroup2, radioGroup3, radioGroup4, radioGroup5, radioGroup6;
    public static String EXPLAIN_TREATMENT = "DID THE DENTIST EXPLAIN YOUR TREATMENT, ANSWER YOUR QUESTIONS?";
    public static String WAITING_TIME = "HOW LONG DID YOU WAIT BEFORE BEING SEEN BY THE DENTIST?";
    public static String RECEPTIONIST_POLITE = "WAS THE RECEPTIONIST HELPFUL, POLITE AND PLEASANT?";
    public static String DENTIST_FRIENDLY = "WAS THE DENTIST ASSISTANT FRIENDLY, SUPPORTIVE AND CONFIDENT?";
    public static String CLINIC_CHAMBER_CLEAN_HYGENIC = "DID THE CLINIC CHAMBER SEEN CLEAN AND HYGIENIC?";
    public static String BILLS_NOT_GIVEN = "BILLS WERE NOT GIVEN ON TIME?";
    private boolean fromAvgFeedback, fromPoorFeedback;
    private static String AVERAGE_URL = "patientDetails/Average Ratings";
    private static String POOR_URL = "patientDetails/Poor Ratings";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anonymous_patient_details);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        findViews();

        childID = getIntent().getStringExtra("firebaseID");
        fromAvgFeedback = getIntent().getBooleanExtra("fromAvgFeedback", false);
        fromPoorFeedback = getIntent().getBooleanExtra("fromPoorFeedback", false);
        id = getIntent().getIntExtra("id", 0);

    }

    public void findViews() {

        radioGroup1 = findViewById(R.id.rg_1);
        radioGroup2 = findViewById(R.id.rg_2);
        radioGroup3 = findViewById(R.id.rg_3);
        radioGroup4 = findViewById(R.id.rg_4);
        radioGroup5 = findViewById(R.id.rg_5);
        radioGroup6 = findViewById(R.id.rg_6);
        comments = findViewById(R.id.comments);
        radioGroup1.setOnCheckedChangeListener(this);
        radioGroup2.setOnCheckedChangeListener(this);
        radioGroup3.setOnCheckedChangeListener(this);
        radioGroup4.setOnCheckedChangeListener(this);
        radioGroup5.setOnCheckedChangeListener(this);
        radioGroup6.setOnCheckedChangeListener(this);

    }

    public DatabaseReference setCurrentFirebaseInstance1() {
        DatabaseReference current_user_db;
        if(fromAvgFeedback){
            databaseReference = FirebaseDatabase.getInstance().getReference(AVERAGE_URL);
            current_user_db = databaseReference.child(childID);
        }else{
            databaseReference = FirebaseDatabase.getInstance().getReference(POOR_URL);
             current_user_db = databaseReference.child(childID);
        }
        return current_user_db.child(EXPLAIN_TREATMENT);
    }

    public DatabaseReference setCurrentFirebaseInstance2() {
        DatabaseReference current_user_db;
        if(fromAvgFeedback){
            databaseReference = FirebaseDatabase.getInstance().getReference(AVERAGE_URL);
            current_user_db = databaseReference.child(childID);
        }else{
            databaseReference = FirebaseDatabase.getInstance().getReference(POOR_URL);
            current_user_db = databaseReference.child(childID);
        }
        return current_user_db.child(WAITING_TIME);
    }

    public DatabaseReference setCurrentFirebaseInstance3() {
        DatabaseReference current_user_db;
        if(fromAvgFeedback){
            databaseReference = FirebaseDatabase.getInstance().getReference(AVERAGE_URL);
            current_user_db = databaseReference.child(childID);
        }else{
            databaseReference = FirebaseDatabase.getInstance().getReference(POOR_URL);
            current_user_db = databaseReference.child(childID);
        }
        return current_user_db.child(RECEPTIONIST_POLITE);
    }

    public DatabaseReference setCurrentFirebaseInstance4() {
        DatabaseReference current_user_db;
        if(fromAvgFeedback){
            databaseReference = FirebaseDatabase.getInstance().getReference(AVERAGE_URL);
            current_user_db = databaseReference.child(childID);
        }else{
            databaseReference = FirebaseDatabase.getInstance().getReference(POOR_URL);
            current_user_db = databaseReference.child(childID);
        }
        return current_user_db.child(DENTIST_FRIENDLY);
    }

    public DatabaseReference setCurrentFirebaseInstance5() {
        DatabaseReference current_user_db;
        if(fromAvgFeedback){
            databaseReference = FirebaseDatabase.getInstance().getReference(AVERAGE_URL);
            current_user_db = databaseReference.child(childID);
        }else{
            databaseReference = FirebaseDatabase.getInstance().getReference(POOR_URL);
            current_user_db = databaseReference.child(childID);
        }
        return current_user_db.child(CLINIC_CHAMBER_CLEAN_HYGENIC);
    }


    public DatabaseReference setCurrentFirebaseInstance6() {
        DatabaseReference current_user_db;
        if(fromAvgFeedback){
            databaseReference = FirebaseDatabase.getInstance().getReference(AVERAGE_URL);
            current_user_db = databaseReference.child(childID);
        }else{
            databaseReference = FirebaseDatabase.getInstance().getReference(POOR_URL);
            current_user_db = databaseReference.child(childID);
        }
        return current_user_db.child(BILLS_NOT_GIVEN);
    }


    public void onClickSubmitButton(View view) {
        commentsText = comments.getText().toString();
        if(commentsText != null) {
            databaseReference.child(childID).child("Comments").setValue(commentsText);
        }
        Intent intent = new Intent(view.getContext(), ThankYouScreen.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        DatabaseReference ref1 = setCurrentFirebaseInstance1();
        DatabaseReference ref2 = setCurrentFirebaseInstance2();
        DatabaseReference ref3 = setCurrentFirebaseInstance3();
        DatabaseReference ref4 = setCurrentFirebaseInstance4();
        DatabaseReference ref5 = setCurrentFirebaseInstance5();
        DatabaseReference ref6 = setCurrentFirebaseInstance6();
        switch (checkedId) {
            case R.id.rb_1:
                ref1.child("1").setValue("EXCELLENT");
                break;

            case R.id.rb_2:
                ref1.child("2").setValue("VERY GOOD");
                break;

            case R.id.rb_3:
                ref1.child("3").setValue("GOOD");
                break;

            case R.id.rb_4:
                ref1.child("4").setValue("AVERAGE");
                break;

            case R.id.rb_5:
                ref1.child("5").setValue("POOR");
                break;

            case R.id.r_1:
                ref2.child("6").setValue("15 MINS");
                break;

            case R.id.r_2:
                ref2.child("7").setValue("30 MINS");
                break;

            case R.id.r_3:
                ref2.child("8").setValue("45 MINS");
                break;

            case R.id.r_4:
                ref2.child("9").setValue("MORE");
                break;

            case R.id.rbutton_1:
                ref3.child("10").setValue("EXCELLENT");
                break;

            case R.id.rbutton_2:
                ref3.child("11").setValue("VERY GOOD");
                break;

            case R.id.rbutton_3:
                ref3.child("12").setValue("GOOD");
                break;

            case R.id.rbutton_4:
                ref3.child("13").setValue("AVERAGE");
                break;

            case R.id.rbutton_5:
                ref3.child("14").setValue("POOR");
                break;

            case R.id.rB_1:
                ref4.child("15").setValue("EXCELLENT");
                break;

            case R.id.rB_2:
                ref4.child("16").setValue("VERY GOOD");
                break;

            case R.id.rB_3:
                ref4.child("17").setValue("GOOD");
                break;

            case R.id.rB_4:
                ref4.child("18").setValue("AVERAGE");
                break;

            case R.id.rB_5:
                ref4.child("19").setValue("POOR");
                break;

            case R.id.radio_1:
                ref5.child("20").setValue("EXCELLENT");
                break;

            case R.id.radio_2:
                ref5.child("23").setValue("VERY GOOD");
                break;

            case R.id.radio_3:
                ref5.child("24").setValue("GOOD");
                break;

            case R.id.radio_4:
                ref5.child("25").setValue("AVERAGE");
                break;

            case R.id.radio_5:
                ref5.child("26").setValue("POOR");
                break;

            case R.id.RB_1:
                ref6.child("27").setValue("YES");
                break;

            case R.id.RB_2:
                ref6.child("28").setValue("NO");
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
