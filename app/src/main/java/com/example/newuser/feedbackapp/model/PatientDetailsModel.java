package com.example.newuser.feedbackapp.model;

import java.util.Date;

public class PatientDetailsModel {

    private int registerationID;
    private String userRating;
    private long timeStamp;

    public PatientDetailsModel() {
    }

    public PatientDetailsModel(int id, String rating, long timeStamp) {
        this.registerationID = id;
        this.userRating = rating;
        this.timeStamp = timeStamp;
    }

    public int getRegisterationID() {
        return registerationID;
    }

    public void setRegisterationID(int registerationID) {
        this.registerationID = registerationID;
    }

    public String getUserRating() {
        return userRating;
    }

    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
