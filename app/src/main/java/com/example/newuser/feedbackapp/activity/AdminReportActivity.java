package com.example.newuser.feedbackapp.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.newuser.feedbackapp.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static com.example.newuser.feedbackapp.activity.FeedbackFormScreen.BILLS_NOT_GIVEN;
import static com.example.newuser.feedbackapp.activity.FeedbackFormScreen.CLINIC_CHAMBER_CLEAN_HYGENIC;
import static com.example.newuser.feedbackapp.activity.FeedbackFormScreen.DENTIST_FRIENDLY;
import static com.example.newuser.feedbackapp.activity.FeedbackFormScreen.EXPLAIN_TREATMENT;
import static com.example.newuser.feedbackapp.activity.FeedbackFormScreen.RECEPTIONIST_POLITE;
import static com.example.newuser.feedbackapp.activity.FeedbackFormScreen.WAITING_TIME;

public class AdminReportActivity extends AppCompatActivity {

    PieChart pieChart;
    long goodCount, avgCount, poorCount;
    DatabaseReference databaseReference;
    DatabaseReference goodReference, avgReference, poorReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_report);
        databaseReference = FirebaseDatabase.getInstance().getReference("patientDetails");
        goodReference = databaseReference.child("Good Ratings");
        avgReference = databaseReference.child("patientDetails/Average Ratings");
        poorReference = databaseReference.child("patientDetails/Poor Ratings");
        pieChart = (PieChart) findViewById(R.id.piechart);
        pieChart.setNoDataText("");
        setPieChart();

        databaseReference.child("Good Ratings").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                goodCount = (int) dataSnapshot.getChildrenCount();
                makePieChart();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        avgReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                makePieChart();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        databaseReference.child("Poor Ratings").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                poorCount = (int) dataSnapshot.getChildrenCount();
                makePieChart();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void setPieChart() {
        pieChart.setUsePercentValues(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.RED);
        pieChart.setTransparentCircleRadius(61f);

    }

    public void makePieChart() {
        ArrayList<PieEntry> yValues = new ArrayList<>();
//        yValues.add(new PieEntry(goodCount, "good"));

        yValues.add(new PieEntry(avgCount, EXPLAIN_TREATMENT));
        yValues.add(new PieEntry(avgCount, WAITING_TIME));
        yValues.add(new PieEntry(avgCount, RECEPTIONIST_POLITE));
        yValues.add(new PieEntry(avgCount, DENTIST_FRIENDLY));
        yValues.add(new PieEntry(avgCount, CLINIC_CHAMBER_CLEAN_HYGENIC));
        yValues.add(new PieEntry(avgCount, BILLS_NOT_GIVEN));
//        yValues.add(new PieEntry(poorCount, "Poor"));

        PieDataSet dataSet = new PieDataSet(yValues, "");

        dataSet.setSliceSpace(6f);
        dataSet.setSelectionShift(6f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieChart.animateY(500);
        Legend l = pieChart.getLegend();
        PieData data = new PieData(dataSet);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.CIRCLE);
        l.setXEntrySpace(10f);
        l.setYEntrySpace(0f);
        l.setYOffset(30f);
        l.setWordWrapEnabled(true);
        l.setDrawInside(false);
        l.getCalculatedLineSizes();
        data.setValueTextSize(40f);
        data.setValueTextColor(Color.BLACK);
        pieChart.setData(data);
        pieChart.highlightValues(null);
        pieChart.invalidate();

    }
}
